from django.shortcuts import render
from django.http import JsonResponse, Http404
from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt


from rest_framework import generics,pagination,status,viewsets
from rest_framework.views import APIView
from rest_framework.response import Response

from UsersDetails.models import Users
from UsersDetails.serializers import UserSerializer
import random
from django.core import serializers
import urllib.request, json 
import requests

import json

def populate_users(request):
    """
    View used to populate Data using given data at 
    http://demo9197058.mockable.io/users
    uses urllib.request
    """

    with urllib.request.urlopen("http://demo9197058.mockable.io/users") as url:
        data = json.loads(url.read().decode())
    
    for item in data:
        p, created = Users.objects.get_or_create(
            id = item['id'],
            first_name = item['first_name'],
            last_name =  item['last_name'],
            company_name = item['company_name'],
            city = item['city'],
            state = item['state'],
            zip = item['zip'],
            email = item['email'],
            web = item['web'],
            age = item['age'],
        )

    # also can use faker for this 
    # for _ in range(500):
    #     p, created = Users.objects.get_or_create(
    #         first_name = fake.first_name(),
    #         last_name =  fake.last_name(),
    #         company_name = fake.company(),
    #         city = fake.city(),
    #         state = fake.state(),
    #         zip = fake.zipcode(),
    #         email = fake.email(),
    #         web = fake.url(),
    #         age = random.randint(1,122)
    #     )
    return JsonResponse({'done':True})

# drf needs class to set the page_size and page_limit, optimize this later.
class UsersListResultsSetPagination(pagination.PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 1000

class UserViewSet(generics.ListAPIView):
    serializer_class = UserSerializer
    # to limit the page items  
    pagination_class = UsersListResultsSetPagination
    # default page limit
    pagination_class.page_size = 5

    def get_queryset(self):
        """
            get for users model with params of limit, page, name and sort
            set limit for pagination_class.page_size,
            set name for filtering first_name contains 'given-name' or last_name contains 'given-name'
            sort is default order_by django filter (-descending)/(ascending)
        """
        queryset = Users.objects.all()
        limit= self.request.query_params.get('limit',None)
        if limit is not None:
            self.pagination_class.page_size = limit
        username = self.request.query_params.get('name', None)
        if username is not None:
            queryset = queryset.filter(Q(first_name__contains = username) | Q(last_name__contains = username))
        sort = self.request.query_params.get('sort', None)
        if sort is not None:
            queryset = queryset.order_by(sort)
        return queryset
    
    @method_decorator(csrf_exempt)
    def post(self, request, format=None):
        """
            defines POST API call for users model
            requires ending / after url i.e. localhost:8000/api/users/
        """
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserDetails(APIView):
    """
        Retrieve, update or delete a user instance
    """
    def get_object(self,id):
        try:
            return Users.objects.get(id=id)
        except Users.DoesNotExist:
            raise Http404
    
    def get(self, request, id, format=None):
        user = self.get_object(id)
        serializer = UserSerializer(user)
        return Response(serializer.data)

    def put(self, request, id, format=None):
        # to-do check put method doc in drf
        user = self.get_object(id)
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, request, id, format=None):
        user = self.get_object(id)
        user.delete()
        return Response(status=status.HTTP_200_OK)