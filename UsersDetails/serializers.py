
from rest_framework import serializers
from UsersDetails.models import Users

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('id','first_name','last_name','company_name','city', 'state', 'zip','email',
                    'web','age')

# this is using serializer class of djangoRF instead used ModelSerializer
# remove this next revisions
# class UserSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     first_name = serializers.CharField(required=False,max_length=30)
#     last_name = serializers.CharField(max_length=150)
#     company_name = serializers.CharField(max_length=150)
#     city = serializers.CharField(max_length=150)
#     state = serializers.CharField(max_length=150)
#     zip = serializers.CharField(max_length=5)
#     email = serializers.EmailField()
#     web = serializers.URLField()
#     age = serializers.CharField()

#     def create(self, validated_data):
#         """
#         Create and return a new `Users` instance, given the validated data.
#         """
#         return Users.objects.create(**validated_data)

#     def update(self, instance, validated_data):
#         """
#         Update and return an existing `Snippet` instance, given the validated data.
#         """
#         instance.first_name = validated_data.get('first_name', instance.first_name)
#         instance.last_name = validated_data.get('last_name', instance.last_name)
#         instance.company_name = validated_data.get('company_name', instance.company_name)
#         instance.city = validated_data.get('city', instance.city)
#         instance.state = validated_data.get('state', instance.state)
#         instance.zip = validated_data.get('zip', instance.zip)
#         instance.email = validated_data.get('email', instance.email)
#         instance.web = validated_data.get('web', instance.web)
#         instance.age = validated_data.get('age', instance.age)
#         instance.save()
#         return instance