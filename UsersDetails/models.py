from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class Users(models.Model):
    first_name = models.CharField(_('First Name'), max_length=30,blank=True)
    last_name = models.CharField(_('Last Name'), max_length=150,blank=True)
    company_name = models.CharField(_('Company Name'), max_length=150,blank=True)
    city = models.CharField(_('city'), max_length=150,blank=True)
    state = models.CharField(_('state'), max_length=150,blank=True)
    zip = models.IntegerField(_('Zip Code'),blank=True)
    email = models.EmailField(_('Email'),blank=True)
    web = models.URLField(_('Web'),blank=True)
    age = models.IntegerField(_('Age'),
        validators=[MaxValueValidator(122), MinValueValidator(1)],
        blank=True
        )

    
