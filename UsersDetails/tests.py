
from django.urls import reverse
from django.test import TestCase

from rest_framework.test import APIClient
from rest_framework import status

from .models import Users


# tests User model
class ModelTestCase(TestCase):
    """This class defines the test suite for the 
    user model.
    """
    def setUp(self):
        """defines data for user model test"""
        self.first_name = "nagendra"
        self.last_name = "singh"
        self.company_name = "vision Books"
        self.city = "kathmandu"
        self.state = "BG3"
        self.zip = 44600
        self.email = "thebestbookfinder@yahoo.com"
        self.web = "thebestbookfinder.com"
        self.age = 58

        self.user = Users(first_name=self.first_name,
            last_name = self.last_name,
            company_name = self.company_name,
            city = self.city,
            # state = self.state, 
            zip = self.zip,
            # email = self.email, 
            # web = self.web, 
            age = self.age
        )

    def test_model_can_create_a_users(self):
        """checks number of users before and after creating new user
        """
        old_count = Users.objects.count()
        self.user.save()
        new_count = Users.objects.count()
        self.assertNotEqual(old_count, new_count)

# testing the user api
class UsersAPI(TestCase):
    """ Test module for GET all puppies API """

    def setUp(self):
        Users.objects.create(
            first_name='john',
            last_name = 'pupuet',
            company_name = 'amazon',
            city = 'californai',
            zip = 44567, 
            age = 29)
        
        Users.objects.create(
            first_name='Karon',
            last_name = 'Smith',
            company_name = 'wells',
            city = 'ojai',
            zip = 41237, 
            age = 23)
        
        Users.objects.create(
            first_name='Karter',
            last_name = 'Blak',
            company_name = 'wills',
            city = 'Chicago',
            zip = 23142, 
            age = 98)
        
        Users.objects.create(
            first_name='Brunie',
            last_name = 'Lyon',
            company_name = 'maiden',
            city = 'southhampton',
            zip = 12567, 
            age = 49)

        self.client = APIClient()
        self.user_data =  {
                "id": 2,
                "first_name": "Josephine",
                "last_name": "Darakjy",
                "company_name": "Chanay, Jeffrey A Esq",
                "city": "Brighton",
                "state": "MI",
                "zip": 48116,
                "email": "josephine_darakjy@darakjy.org",
                "web": "http://www.chanayjeffreyaesq.com",
                "age": 48
            }
        self.response = self.client.post(
            reverse('users_list'),
            self.user_data,
            format="json")
        
    def test_api_can_create_a_user(self):
        """Test the api has user creation capability."""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    
    def test_api_can_get_a_userslist(self):
        """Test the api can get a given userslist."""
        userlist = Users.objects.get(id = 2)
        response = self.client.get(
            reverse('user_details',
            kwargs={'id': userlist.id}), format="json")
        print('test response',response.status_code)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # self.assertContains(response, userlist)

    def test_api_can_update_bucketlist(self):
        """Test the api can update a given user."""
        change_userlist = {
                "first_name": "Josephine",
                "last_name": "Darakjy",
                "age": 48 }
        res = self.client.put(
            reverse('user_details', kwargs={'id': 2}),
            change_userlist, format='json'
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_api_can_delete_user(self):
        """Test the api can delete a user."""
        response = self.client.delete(
            reverse('user_details', kwargs={'id': 2}),
            format='json',
            follow=True)

        self.assertEquals(response.status_code, status.HTTP_200_OK)