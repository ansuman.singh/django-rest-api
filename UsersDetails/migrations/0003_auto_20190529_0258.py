# Generated by Django 2.2.1 on 2019-05-29 02:58

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('UsersDetails', '0002_auto_20190528_1709'),
    ]

    operations = [
        migrations.AlterField(
            model_name='users',
            name='age',
            field=models.IntegerField(blank=True, validators=[django.core.validators.MaxValueValidator(122), django.core.validators.MinValueValidator(1)], verbose_name='Age'),
        ),
        migrations.AlterField(
            model_name='users',
            name='city',
            field=models.CharField(blank=True, max_length=150, verbose_name='city'),
        ),
        migrations.AlterField(
            model_name='users',
            name='company_name',
            field=models.CharField(blank=True, max_length=150, verbose_name='Company Name'),
        ),
        migrations.AlterField(
            model_name='users',
            name='email',
            field=models.EmailField(blank=True, max_length=254, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='users',
            name='first_name',
            field=models.CharField(blank=True, max_length=30, verbose_name='First Name'),
        ),
        migrations.AlterField(
            model_name='users',
            name='last_name',
            field=models.CharField(blank=True, max_length=150, verbose_name='Last Name'),
        ),
        migrations.AlterField(
            model_name='users',
            name='state',
            field=models.CharField(blank=True, max_length=150, verbose_name='state'),
        ),
        migrations.AlterField(
            model_name='users',
            name='web',
            field=models.URLField(blank=True, verbose_name='Web'),
        ),
        migrations.AlterField(
            model_name='users',
            name='zip',
            field=models.IntegerField(blank=True, verbose_name='Zip Code'),
        ),
    ]
