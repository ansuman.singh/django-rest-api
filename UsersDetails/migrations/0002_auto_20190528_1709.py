# Generated by Django 2.2.1 on 2019-05-28 17:09

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('UsersDetails', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='users',
            name='age',
            field=models.IntegerField(validators=[django.core.validators.MaxValueValidator(122), django.core.validators.MinValueValidator(1)], verbose_name='Age'),
        ),
        migrations.AlterField(
            model_name='users',
            name='zip',
            field=models.IntegerField(verbose_name='Zip Code'),
        ),
    ]
