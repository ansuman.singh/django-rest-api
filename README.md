# Django Rest API

To do:
-- Add tests(done)
-- check code(done)
-- validate against the original mock


## installation
    1. git clone https://gitlab.com/ansuman.singh/django-rest-api.git
    2. create virtualenv(recommended) $: virtualenv -p python3 venv and activate venv
    3. cd django-rest-api 
    4. pip install -r requirements.txt
    5. remove the existing database $: rm db.sqlite3 (for fresh installation)
    6. python manage.py migrate
    7. python manage.py test
    8. python manage.py runserver
    9. open http://127.0.0.1:8000/populate/users/ to populate data form mock site
    10. api is available on http://127.0.0.1:8000/api/users/ for GET and POST
        http://127.0.0.1:8000/api/users/1/ for GET, PUT, DELETE operations.
    

### Usage:
#### /api/users - GET - To list the users
    use http://127.0.0.1:8000/api/users/ 
for this operation, currently it is set to return with a limit of 5 users with pagination as specified in the assesment this but can be changed by changing django view- UserViewSet where value of pagination_class.page_size is set to 5

output:
```javascript
{
    "count": 502,
    "next": "http://127.0.0.1:8000/api/users/?page=2",
    "previous": null,
    "results": [
        {
            "id": 1,
            "first_name": "James",
            "last_name": "Butt",
            "company_name": "Benton, John B Jr",
            "city": "New Orleans",
            "state": "LA",
            "zip": 70116,
            "email": "jbutt@gmail.com",
            "web": "http://www.bentonjohnbjr.com",
            "age": 70
        },
        {
            "id": 2,
            "first_name": "Josephine",
            "last_name": "Darakjy",
            "company_name": "Chanay, Jeffrey A Esq",
            "city": "Brighton",
            "state": "MI",
            "zip": 48116,
            "email": "josephine_darakjy@darakjy.org",
            "web": "http://www.chanayjeffreyaesq.com",
            "age": 48
        },
        {
            "id": 3,
            "first_name": "Art",
            "last_name": "Venere",
            "company_name": "Chemel, James L Cpa",
            "city": "Bridgeport",
            "state": "NJ",
            "zip": 80514,
            "email": "art@venere.org",
            "web": "http://www.chemeljameslcpa.com",
            "age": 80
        },
        {
            "id": 4,
            "first_name": "Lenna",
            "last_name": "Paprocki",
            "company_name": "Feltz Printing Service",
            "city": "Anchorage",
            "state": "AK",
            "zip": 99501,
```
#### supports query parameters
    localhost:8000/api/users?page=1&limit=10&name=James&sort=-age


```javascript
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "first_name": "James",
            "last_name": "Butt",
            "company_name": "Benton, John B Jr",
            "city": "New Orleans",
            "state": "LA",
            "zip": 70116,
            "email": "jbutt@gmail.com",
            "web": "http://www.bentonjohnbjr.com",
            "age": 70
        }
    ]
}
```

### /api/users - POST - To create a new user
    
    data = 
        {
            "id": 508,
            "first_name": "123simona",
            "last_name": "123Morasca",
            "company_name": "123Chapman, Ross E Esq",
            "city": "Ashland123",
            "state": "O123H",
            "zip": 44812305,
            "email": "sim123ona@morasca.com",
            "web": "http://123www.chapmanrosseesq.com",
            "age": 42
        }

``` javascript
HTTP 201 Created
Allow: GET, POST, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "id": 508,
    "first_name": "123simona",
    "last_name": "123Morasca",
    "company_name": "123Chapman, Ross E Esq",
    "city": "Ashland123",
    "state": "O123H",
    "zip": 44812305,
    "email": "sim123ona@morasca.com",
    "web": "http://123www.chapmanrosseesq.com",
    "age": 42
}

```

#### /api/users/{id} - GET - To get the details of a user
    http://127.0.0.1:8000/api/users/2/

output
```javascript
    HTTP 200 OK
Allow: GET, PUT, DELETE, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "id": 2,
    "first_name": "Micheal",
    "last_name": "Deaver",
    "company_name": "Chanay, Jeffrey A Esq",
    "city": "Brighton",
    "state": "MI",
    "zip": 48116,
    "email": "josephine_darakjy@darakjy.org",
    "web": "http://www.chanayjeffreyaesq.com",
    "age": 12
}
```
#### /api/users/{id} - PUT To update the details of a user
    http://127.0.0.1:8000/api/users/2/

data = {
    "first_name": "Josephine",
    "last_name": "Darakjy",
    "age": 48
}

output:
```javascript
HTTP 200 OK
Allow: GET, PUT, DELETE, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "id": 2,
    "first_name": "Josephine",
    "last_name": "Darakjy",
    "company_name": "Chanay, Jeffrey A Esq",
    "city": "Brighton",
    "state": "MI",
    "zip": 48116,
    "email": "josephine_darakjy@darakjy.org",
    "web": "http://www.chanayjeffreyaesq.com",
    "age": 48
}
```

/api/users/{id} - DELETE
    http://127.0.0.1:8000/api/users/2/

output:
```javascript
HTTP 200 OK
Allow: GET, PUT, DELETE, HEAD, OPTIONS
Content-Type: application/json
Vary: Accept
```

Thank you -ansuman :innocent: